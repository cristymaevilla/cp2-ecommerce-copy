const jwt= require("jsonwebtoken");

const secret= "ecommerceAPI"

// TOKEN creation:
module.exports.createAccessToken= (user)=>{
	// the data will be receieved from the registration form
	// When user logs in, a token will be created w/ the user's info.
	const data={
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin	
	};
	// generates a JSON web token using jwt's sign method:
	return jwt.sign(data,secret, {});
}

// Token Verification-------------------------------------
module.exports.verify=(req, res, next) => {
	let token = req.headers.authorization

	if (typeof token !== "undefined"){
		console.log(token);
		// you can exclude console.log (above)
		//we use .slice to exclude " Bearer " from the json web token provided per user that logs in
		token = token.slice(7, token.length)

		// validate the token using the verify method decrypting the token using the SECRET code
		return jwt.verify(token, secret, (err, data)=>{

			// if JWT is invalid / JWT exists but ur not authorized to use it
			if (err){
				return res.send({auth: "failed"});
			}
			else{
				// allows the app to proceed with the next middleware function/ callback function in the ROUTE
				next();
			}
		})
	}
	// if token DOES NOT exist
	else{
		return res.send({auth : "failed"});
	}
}

// DECRYPT JSON web token to obtain info-------------------
module.exports.decode =(token)=> {
	if (typeof token !== "undefined"){
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (err,data)=> {
			if(err){return null;}
			// payload-to get user's info from create access token
			else{ return jwt.decode(token, {complete:true}).payload;}
		})
	}
	else {return null;}
}