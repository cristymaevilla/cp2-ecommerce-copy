
const Product = require("../models/modproduct");

const auth= require("../auth");


// CREATE a product (ADMIN ONLY)------------------------ok
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			category: reqBody.category,
			genre: reqBody.genre,
			price: reqBody.price,
			stocks: reqBody.stocks
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				return ('Product sucessfully added!');
			}
		})
	}
	else{
		return (`You have no access`);
	}
}

// RETRIEVE ALL ACTIVE products---------------------------ok
module.exports.getAllActiveProducts = ()=> {
		return Product.find({isActive : true}).then(result => {
			return result;
		})
}

// RETRIEVE by category---------------------------ok
module.exports.getByCategory = (reqBody)=> {
	
		return Product.find(reqBody).then(result => {
			return result;
		})
}



// RETRIEVE SINGLE product:------------------------------ok
module.exports.getProduct= async (reqParams)=>{
	
		let product=await Product.findById(reqParams.productId).then(result =>{
		return result;
		});

		console.log(product);
		let reviews = product.reviews
			console.log(reviews);
		// let ave= reviews.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
		// ave= parseInt(ave.rating/reviews.length)
			// console.log(ave);
		// let reviewCount= {totalReviews: reviews.length};
			// console.log(reviews.length);
		// let data={reviews:reviews.length, averageRating:ave};
	if(reviews == ""){return {  
					Reviews:"no reviews yet",
					averageRating:"no ratings yet",
					product}}
	else { let ave= reviews.reduce((acc,curr)=> ({rating: acc.rating + curr.rating}));
			ave= parseInt(ave.rating/reviews.length);
			console.log(ave);
		return {  Reviews:reviews.length,averageRating:ave,product}
	}
}

/*module.exports.getProduct= async (reqParams)=>{







	if(reviews.length >= 0){return product}
}*/
// UPDATE product information (ADMIN only):--------------ok
module.exports.updateProduct= async (user, reqParams, reqBody)=>{
	if(user.isAdmin){
		// Specify the fields of the doc. to be updated
		let updatedProduct = {
			name: reqBody.name,
			Author: reqBody.author,
			category: reqBody.category,
			genre: reqBody.genre,
			description:reqBody.description,
			price: reqBody.price,
			stocks:reqBody.stocks
		}
		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product,error)=>{
			if(error){return false;}
			else{return ('Product sucessfully updated!');}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}

// ARCHIVE product (ADMIN only)--------------------------ok
module.exports.archiveProduct= async (user, reqParams)=>{
	if(user.isAdmin){
		return Product.findByIdAndUpdate(reqParams.productId, {isActive :false} ).then((course,error)=>{
			
			if(error){return false;}
			else{return ('You have archived this product');}
		})
	}
	else{
		return (`Sorry, you have no access`);
	}
}

// ADD REVIEWS by USER:----------------------------------ok
module.exports.addReview= async (user, reqParams, reqBody)=>{
	if(user.isAdmin){return (`Sorry, you have no access`) }	
	else{
		let review = {
			userId: user.id,
			rating: reqBody.rating,
			comment: reqBody.comment,}
console.log(user.id);
		if(reqBody.rating < 0 || reqBody.rating > 5){return ("Oops! Rating must be from 1 to 5")}
		else{
			return Product.findById(reqParams.productId).then(product => {
			product.reviews.push(review);
			return product.save().then((user, error) => {
				if(error){return false;}
				else{return ('Review posted');}
			})
		})

		}
		
}
}

 // --------------------------------------------ok
module.exports.deleteReview= async (user, reqParams)=>{
	if(user.isAdmin){return (`Sorry, you have no access`) }	
	else{
		console.log(reqParams.reviewId);
		return Product.updateOne({_id:reqParams.productId}, {$pull: {reviews:{_id:reqParams.reviewId}}}).then((user, error) => {
				if(error){return false;}
				else{return ('Review deleted');}
			})

	}
}