const express = require("express");
const router = express.Router();

const orderController= require("../controllers/order");
const auth= require("../auth");

// MAKEORDER-------------------------USE THIS FOR NOW

router.post("/:itemId/users/checkout", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.createOrder(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// update user orders=======================
/*router.put("/:orderId/user", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.updateUserOrders(user, req.params).then(resultFromController => res.send(resultFromController));
});*/

// update user orders and cart =======================
router.put("/:orderId/confirm", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.updateUserOrdersCart(user, req.params).then(resultFromController => res.send(resultFromController));
});

// update products =======================
router.put("/:orderId/product", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.updateProduct(user, req.params).then(resultFromController => res.send(resultFromController));
});



//Cancel order==========================
router.put("/:orderId/cancel", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	orderController.cancel(user, req.params).then(resultFromController => res.send(resultFromController));
});

// Find user's orders exclude cancelled orders-------
router.get("/users/orders", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	orderController.all(user).then(resultFromController => res.send(
		resultFromController));
})
// GET ALL ORDERS by admin----ok
router.get("/", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	orderController.allOrders(user).then(resultFromController => res.send(
		resultFromController));
})
// GET USER'S ORDERS BY ADMIN reqbody:users id-----ok
router.get("/users/myOrders", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	orderController.usersOrders(user, req.body).then(resultFromController => res.send(
		resultFromController));
})
// retrieve all completed orders:
/*router.get("/completed", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	orderController.retrieveCarts(user).then(resultFromController => res.send(
		resultFromController));
})*/


// retrieve specific order by user----------------ok
router.get("/:orderId", auth.verify, (req,res)=>{
	const user = auth.decode(req.headers.authorization)

	orderController.retrieveOrder(user, req.params).then(resultFromController => res.send(
		resultFromController));
})


// Update status of order by admin-----ok
router.put("/:orderId/status", auth.verify,(req,res)=>{
	const user= auth.decode(req.headers.authorization);
	orderController.updateComplete(user,req.params, req.body).then(resultFromController=> res.send(resultFromController));
})












module.exports=router